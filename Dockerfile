FROM node:16

# Create app directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Build code (not in production since we need vue-cli)
RUN npm ci

# Bundle app source
COPY . .

# Build Nuxt
RUN npm run build

# TODO: ENV

# Runtime
CMD ["npm", "start"]
