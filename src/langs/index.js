import en from './en'
import es from './es'

export const defaultLocale = 'en'

export const languages = {
  en: en,
  es: es
}
