import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { languages, defaultLocale } from '../langs/index'

Vue.use(VueI18n)

// Copy and reName the languages object
const messages = Object.assign(languages)

// Sets the default lang and the translations
const opts = {
  locale: defaultLocale,
  messages
}

export default new VueI18n(opts)
