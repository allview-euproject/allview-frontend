# ALLVIEW FRONTEND
### This repo contains the Frontend of the ALLVIEW Platform, written in Vue using Vuetify and CarbonUI

<br>

## Project overview
- public/: public files like Html and images
- src/main.js: imports plugins and launches the Vue app
- src/App.vue: app entry point, it just calls the router
- src/router/: contains all the routes, with their layouts and components
- src/plugins/: configs of the Vue plugins
- src/layouts/: contains the layouts (toolbar and footer, essentially) for the routes
- src/pages/: contains all the style, structure, and logic for the content of each route
- src/components/: pieces of Vue code that can be imported in other Vue files, like Layouts or Pages
- src/assets/: files such as scss and images
- src/langs/: internationalization files (not used)
- src/configs/: CarbonUI stuff
- src/filters/: CarbonUI stuff
- src/store/: CarbonUI stuff
- dist/: app compiled and minified app production

<br>

## Documentation
- [Vue](https://vuejs.org/v2/guide/)
- [Vuetify](https://vuetifyjs.com/)
- CarbonUI (private, please ask for the files)

<br>

## Installation

Nodejs 12 or higher with NPM must be installed. You can download the latest stable release for your operating system [here](https://nodejs.org/en/download/).

Install all the project dependencies
```
npm install
```

<br>

## Configuration
Copy the `example.env.local` file to `.env.local` and set your custom environment variables.
```
cp example.env.local .env.local
nano .env.local
```

<br>

## Run for development
Compiles and hot-reloads for development
```
npm run dev
```
or
```
npm run serve
```

<br>

## Compile for production
Compiles and minifies the app for production
```
npm run build
```
